// vim: set sw=4 sts=4 et:
// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

const Clutter = imports.gi.Clutter;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;

function getKernelVersion() {
    let version = null;
    let content = null;

    content = Shell.get_file_contents_utf8_sync('/proc/version');

    if (content.indexOf('Debian') != -1) {
        /* Debian: get the version from the end of /proc/version:
         *
         *     $ cat /proc/version
         *     ... <much stuff here> ... #1 SMP Debian 5.4.6-1 (2019-12-27)
         */
        let elems = content.split(' ');
        if (elems.length > 2)
            version = elems[elems.length - 2];
    }

    if (version == null) {
        /* Otherwise: get the version from the beginning of /proc/version:
         *
         *     $ cat /proc/version
         *     Linux version 5.4.5-arch1-1.1 ... <much stuff here> ...
         */
        let elems = content.split(' ');
        if (elems.length > 3)
            version = elems[2];
    }

    return version;
}

class KernelButton extends PanelMenu.Button {
    constructor() {
        super(0.0, 'Kernel Indicator');

        let version = getKernelVersion();
        if (version == null) {
            version = 'Linux';
        }

        let label = new St.Label({ text: version,
                                   y_expand: true,
                                   y_align: Clutter.ActorAlign.CENTER });
        this.actor.add_actor(label);	
    }
};

let _indicator;

function enable() {
    _indicator = new KernelButton;
    Main.panel.addToStatusArea('kernel-indicator', _indicator);
}

function disable() {
    _indicator.destroy();
}
